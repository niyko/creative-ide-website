<?php
    require_once("./env.php");
    require_once("./frameworks/db-framework.php");
    require_once("./frameworks/db-connection.php");
    $version_info = $db_connection->where("version", $_GET["version"])->getOne("cide_version_history");
    if($version_info==null){
        echo "<script>location = '../version';</script>";
        exit();
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>What's new <?php echo $_GET["version"]; ?> | Creative IDE</title>
        <meta name="description" content="See the new features introduced in the V<?php echo $_GET["version"]; ?> of Creative IDE. Download the private alpha and beta versions of the app"/>
        <!----- Header ----->
        <?php require_once("./elements/header.php"); ?>
    </head>
    <body>
        <!----- Top Nav Bar ----->
        <?php require_once("./elements/top-nav-bar.php"); ?>

        <div class="subpage-header">
            <div class="subpage-header-inner"></div>
            <a href="<?php echo $env_host; ?>"><img class="header-logo" src="images/logo-color.png"></a>
        </div>

        <div class="subpage-content">
            <div class="uk-container uk-container-xsmall">
                <h1 class="subpage-title uk-margin-bottom">Whats new in <?php echo $_GET["version"]; ?></h1>
                <p class="subpage-para"><?php 
                    $bits = explode("\n", $version_info["whatsnew"]);
                    $list_html = "<ul>";
                    foreach($bits as $bit) $list_html .= "<li>" . $bit . "</li>";
                    $list_html .= "</ul>";
                    echo $list_html;
                ?></p>
                <div class="uk-flex uk-flex-center uk-flex-left@s">
                    <a href="<?php echo $version_info["link"]; ?>"><button class="btn-primary ripples uk-margin-top" data-color="#3848d4" data-opacity="0.3">Download</button></a>
                </div>

                <a class="subpage-nextpage" href="version">Other versions <i class="material-icons">keyboard_arrow_right</i></a>
            </div>
        </div>

        <!----- Footer ----->
        <?php require_once("./elements/footer.php"); ?>

        <!----- Copyright ----->
        <?php require_once("./elements/copyright.php"); ?>
        
        <!----- Scripts ----->
        <?php require_once("./elements/scripts.php"); ?>
        <script>
            
        </script>
    </body>
</html>