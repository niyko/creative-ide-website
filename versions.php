<?php
    require_once("./env.php");
    require_once("./frameworks/db-framework.php");
    require_once("./frameworks/db-connection.php");
    $versions = $db_connection->orderBy("cide_version_history.id","desc")->get("cide_version_history");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Versions | Creative IDE</title>
        <meta name="description" content="List of all production, beta and alpha versions of the Cretive IDE app and what's new on each release"/>
        <!----- Header ----->
        <?php require_once("./elements/header.php"); ?>
    </head>
    <body>
        <!----- Top Nav Bar ----->
        <?php require_once("./elements/top-nav-bar.php"); ?>

        <div class="subpage-header">
            <div class="subpage-header-inner"></div>
            <a href="<?php echo $env_host; ?>"><img class="header-logo" src="images/logo-color.png"></a>
        </div>

        <div class="subpage-content">
            <div class="uk-container uk-container-xsmall">
                <h1 class="subpage-title uk-margin-bottom">All versions</h1>
                <h2 class="subpage-sidenote">List all versions and features that introduced in it</h2>
                <p class="subpage-para"><?php 
                    foreach($versions as $version){
                        echo "<a href='version/${version["version"]}' class='subpage-text-link uk-margin-left' href=''>${version["version"]}</a>";
                    }
                ?></p>
            </div>
        </div>

        <!----- Footer ----->
        <?php require_once("./elements/footer.php"); ?>

        <!----- Copyright ----->
        <?php require_once("./elements/copyright.php"); ?>
        
        <!----- Scripts ----->
        <?php require_once("./elements/scripts.php"); ?>
        <script>
            
        </script>
    </body>
</html>