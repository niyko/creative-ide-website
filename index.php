<?php
    require_once("./env.php");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Creative IDE | Android App</title>
        <meta name="description" content="Creative IDE is an integrated development environment and a learning platform for 64 programming language. Your can run your code with one click from the IDE.">
        <!----- Header ----->
        <?php require_once("./elements/header.php"); ?>
    </head>
    <body>
        <!----- Top Nav Bar ----->
        <?php require_once("./elements/top-nav-bar.php"); ?>

        <!----- Header Section One ----->
        <div class="header-section">
            <div uk-grid class="header-section-grid">
                <div class="header-section-grid-column uk-width-1-3 uk-flex uk-flex-middle uk-flex-right hide-in-mobile-flex">
                    <img class="screenshot-img" uk-parallax="y: 0,-70;" src="images/screenshot-1.png">
                </div>
                <div class="header-section-grid-column uk-width-auto uk-flex uk-flex-middle uk-flex-left">
                    <div class="header-content-section" uk-parallax="y: 0,-120;">
                        <h1 class="header-title">Creative<span class="header-title-tail">.</span></h1>
                        <p class="header-subtitle">Creative IDE is an integrated development environment and a learning platform for 64 programming language. Your can run your code with one click from the IDE.</p>
                        <a href="https://play.google.com/store/apps/details?id=com.creativeide.niyko.creativeide" target="_blank"><button class="download-btn ripples" data-color="white" data-opacity="0.3">Download</button></a>
                        <a class="version-text uk-margin-small-left" href="version/1.0.3beta">V1.0.3 beta</a>
                    </div>
                </div>
            </div>
            <img class="screenshot-img-mobile hide-in-pc" uk-parallax="y: 0,-100;" src="images/screenshot-1.png">
            <a href="<?php echo $env_host; ?>"><img class="header-logo" src="images/logo-color.png"></a>
        </div>

        <!----- Footer ----->
        <?php require_once("./elements/footer.php"); ?>

        <!----- Copyright ----->
        <?php require_once("./elements/copyright.php"); ?>
        
        <!----- Scripts ----->
        <?php require_once("./elements/scripts.php"); ?>
        <script>
            
        </script>
    </body>
</html>