<?php
    require_once("./env.php");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Creative IDE</title>
        <!----- Header ----->
        <?php require_once("./elements/header.php"); ?>
    </head>
    <body>
        <!----- Top Nav Bar ----->
        <?php require_once("./elements/top-nav-bar.php"); ?>

        <div class="subpage-header">
            <div class="subpage-header-inner"></div>
            <img class="header-logo" src="images/logo-color.png">
        </div>

        <div class="subpage-content">
            <div class="uk-container uk-container-xsmall">
                <h1 class="subpage-title">Lorem ipsum dolor sit amet consectetur</h1>
                <h2 class="subpage-subtitle">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloribus nostrum iste ut voluptas.</h2>
                <p class="subpage-para">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem tenetur perspiciatis ex dolores doloremque, sapiente molestiae itaque id veritatis accusamus nihil fugiat corrupti ducimus animi, rem ipsum incidunt voluptatum dolor! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem tenetur perspiciatis ex dolores doloremque, sapiente molestiae itaque id veritatis accusamus nihil fugiat corrupti ducimus animi, rem ipsum incidunt voluptatum dolor!</p>
            </div>
        </div>

        <!----- Footer ----->
        <?php require_once("./elements/footer.php"); ?>

        <!----- Copyright ----->
        <?php require_once("./elements/copyright.php"); ?>
        
        <!----- Scripts ----->
        <?php require_once("./elements/scripts.php"); ?>
        <script>
            
        </script>
    </body>
</html>