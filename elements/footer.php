<div class="uk-section uk-section-primary footer">
    <div class="uk-container uk-container-small">
        <div>
            <div uk-grid class="uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-4@m">
                <div>
                    <img class="footer-logo" src="images/logo-white.png">
                </div>
                <div>
                    <ul class="footer-links">
                        <li class="footer-links-title">More links</li>
                        <li><a href="https://trello.com/b/oG45qPh2" target="_blank">Todo features</a></li>
                        <li><a href="https://forms.gle/VGgUaPvGDyzBnz5p8" target="_blank">Feature request</a></li>
                        <li><a href="https://forms.gle/UAXz5LaJjVkSBHMT8" target="_blank">Report issue</a></li>
                        <li><a href="https://t.me/creative_ide" target="_blank">Join group</a></li>
                    </ul>
                </div>
                <div>
                    <ul class="footer-links">
                        <li class="footer-links-title">Resources</li>
                        <li><a href="<?php echo $env_host; ?>privacy">Privacy Policy</a></li>
                        <li><a href="<?php echo $env_host; ?>terms">Terms & Conditions</a></li>
                        <li><a href="mailto:hello@niyko.com">Get in touch</a></li>
                    </ul>
                </div>
                <div>
                    <ul class="footer-links">
                        <li class="footer-links-title">Social</li>
                        <li><a href="https://www.facebook.com/creativeideofficial" target="_blank">Facebook</a></li>
                        <li><a href="https://twitter.com/creativeide_app" target="_blank">Twitter</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>