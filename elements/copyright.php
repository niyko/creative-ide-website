<div class="uk-section uk-section-primary copyright-container">
    <div class="uk-container uk-container-small">
        <div uk-grid>
            <div class="uk-width-expand uk-flex uk-flex-middle">
                <p class="copyright-text"><span>&copy; Creative IDE 2020</span> All rights reserved.</p>
            </div>
            <div class="uk-width-auto">
                <img src="images/footer-niyko-logo.png" class="copyright-logo">
            </div>
        </div>
    </div>
</div>