<div class="top-nav-bar">
    <div uk-grid class="grid">
        <div class="uk-width-expand"></div>
        <div class="uk-width-auto hide-in-mobile">
            <a class="top-nav-bar-link <?php echo (basename($_SERVER['PHP_SELF'])=='index.php')?'active':''; ?>" href="<?php echo $env_host; ?>">Home</a>
        </div>
        <div class="uk-width-auto hide-in-mobile">
            <a class="top-nav-bar-link <?php echo (basename($_SERVER['PHP_SELF'])=='versions.php')?'active':''; ?>" href="<?php echo $env_host; ?>version">What's new</a>
        </div>
        <div class="uk-width-auto hide-in-mobile">
            <a class="top-nav-bar-link" href="https://t.me/creative_ide" target="_blank">Join group</a>
        </div>
        <div class="uk-width-auto hide-in-pc">
            <button class="top-nav-bar-ham-btn ripples"><i class="material-icons">menu</i></button>
            <div uk-dropdown="mode: click" class="top-nav-bar-menu">
                <ul class="uk-nav uk-dropdown-nav">
                    <li class="uk-nav-header">More</li>
                    <li><a href="<?php echo $env_host; ?>version">What's new</a></li>
                    <li><a href="https://trello.com/b/oG45qPh2" target="_blank">Todo features</a></li>
                    <li><a href="https://t.me/creative_ide" target="_blank">Join group</a></li>
                    <li class="uk-nav-header">Legal</li>
                    <li><a href="<?php echo $env_host; ?>terms">Terms</a></li>
                    <li><a href="<?php echo $env_host; ?>privacy">Privacy</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>